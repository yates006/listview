﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

/** Images courtesy of Reverb.com **/

namespace ListView
{
    public partial class ListViewPage : ContentPage
    {
        public ListViewPage()
        {
            InitializeComponent();
            ObservableCollection<Guitar> guitars = new ObservableCollection<Guitar>();
            GuitarView.ItemsSource = guitars;

            guitars.Add(new Guitar
            {
                DisplayName = "Les Paul",
                SubText = "Gibson",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--QaoX8wMR--/a_270,c_crop,h_0.563,w_1.000,x_0.000,y_0.437/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1490132965/qnnuoxe33cyfddufp3aq.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/Gibson_Les_Paul"
            });

            guitars.Add(new Guitar
            {
                DisplayName = "Telecaster",
                SubText = "Fender",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--Bi5rrkhi--/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1505877234/bnl4vsrcp6zpybuuhzmv.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/Fender_Telecaster"
            });

            guitars.Add(new Guitar
            {
                DisplayName = "Stratocaster",
                SubText = "Fender",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--uecwANDB--/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1511727763/twwgkeuz7ahodlhyguu3.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/Fender_Stratocaster"
            });

            guitars.Add(new Guitar
            {
                DisplayName = "Soloist",
                SubText = "Jackson",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--WZBRvKo9--/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1512084718/ryfmadmqew5pmmbr0zcp.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/Jackson_Soloist"
            });

            guitars.Add(new Guitar
            {
                DisplayName = "Warlock",
                SubText = "BC Rich",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--vK51S9KT--/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1485987546/tjjgpd7tsqaunnkjtio0.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/B.C._Rich#Warlock"
            });

            guitars.Add(new Guitar
            {
                DisplayName = "SG",
                SubText = "Gibson",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--7m9xjPKA--/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1504305146/rugdzpke6tn1dg8lqgdq.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/Gibson_SG"
            });

            guitars.Add(new Guitar
            {
                DisplayName = "6120DC",
                SubText = "Gretsch",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--r46aXA8l--/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1493119237/lp8llhgo5dsxrikghqrh.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/Gretsch"
            });

            guitars.Add(new Guitar
            {
                DisplayName = "360",
                SubText = "Rickenbacker",
                Icon = ImageSource.FromUri(new Uri("https://images.reverb.com/image/upload/s--2v369wjN--/a_exif,c_thumb,f_jpg,fl_progressive,g_south,h_96,q_auto:eco,w_96/v1369881842/ohdcn3stlzugoht1w7xb.jpg")),
                LinkURI = "https://en.wikipedia.org/wiki/Rickenbacker_360"
            });
        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            //if (e.SelectedItem == null)
            //{
            //    return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            //}

            var guitar = (Guitar)e.SelectedItem;

            Device.OpenUri(new Uri(guitar.LinkURI));
            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }
    }

    public class GuitarView
    {

    }

    public class GuitarRow
    {

    }

    public class Guitar
    {
        public string DisplayName { get; set; }
        public string SubText { get; set; }
        public ImageSource Icon { get; set; }
        public string LinkURI { get; set; }
    }
}
